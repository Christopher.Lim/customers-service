# Customer Service

## Description

Customer Service is made using the NestJS CLI. And the purpose of this project is to demonstrate to accumulative knowledge that I gain from different resources.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# Contract Test
$ npm run test:contract

# test coverage
$ npm run test:cov
```
