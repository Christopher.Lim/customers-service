import { Injectable, NotFoundException } from '@nestjs/common';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  private customers: Customer[] = [
    {
      id: 1,
      firstName: 'Chris',
      lastName: 'Lim',
      title: 'Test-Ops',
      location: ['USA', 'Philippines'],
    },
    {
      id: 2,
      firstName: 'Marianne',
      lastName: 'Lim',
      title: 'Nurse',
      location: ['Singapore', 'Philippines'],
    },
    {
      id: 3,
      firstName: 'John',
      lastName: 'Doe',
      title: 'Developer',
      location: ['Unknown'],
    },
    {
      id: 4,
      firstName: 'Jane',
      lastName: 'Doe',
      title: 'HR',
      location: ['Unknown'],
    },
  ];

  findAll() {
    return this.customers;
  }

  findOne(id: string) {
    const customer = this.customers.find((item) => item.id === +id);
    if (!customer) {
      throw new NotFoundException(`Customer #${id} not found`);
    }
    return customer;
  }

  create(createCustomerDto: any) {
    this.customers.push(createCustomerDto);
    return createCustomerDto;
  }

  update(id: string, updateCustomerDto: any) {
    const existingCustomer = this.findOne(id);
    if (existingCustomer) {
      return this.customers.splice(updateCustomerDto);
    }
    return new NotFoundException(`Customer #${id} not found`);
  }

  remove(id: string) {
    const customerIndex = this.customers.findIndex((item) => item.id === +id);
    if (customerIndex >= 0) {
      this.customers.splice(customerIndex, 1);
    }
  }
}
