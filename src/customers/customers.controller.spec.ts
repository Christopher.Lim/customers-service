import { Test, TestingModule } from '@nestjs/testing';
import { CustomersController } from './customers.controller';
import { CustomersService } from './customers.service';
import { UpdateCustomerDto } from './dto/update-customer.dto';

describe('CustomersController', () => {
  let controller: CustomersController;
  let service: CustomersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CustomersService],
      controllers: [CustomersController],
    }).compile();

    controller = module.get<CustomersController>(CustomersController);
    service = module.get<CustomersService>(CustomersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of customers', async () => {
      const result = [
        {
          firstName: 'Chris',
          id: 1,
          lastName: 'Lim',
          location: ['USA', 'Philippines'],
          title: 'Test-Ops',
        },
      ];

      jest.spyOn(service, 'findAll').mockImplementation(() => result);
      expect(await controller.findAll()).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return customers with id # 1', async () => {
      const result = {
        id: 1,
        firstName: 'Chris',
        lastName: 'Lim',
        location: ['USA', 'Philippines'],
        title: 'Test-Ops',
      };

      jest.spyOn(service, 'findOne').mockImplementation(() => result);
      expect(await controller.findOne('1')).toBe(result);
    });
  });

  describe('create', () => {
    it('should create a new customer', async () => {
      const result = {
        id: 5,
        firstName: 'Test',
        lastName: 'User',
        location: ['Boston'],
        title: 'Test-Ops',
      };

      jest.spyOn(service, 'create').mockImplementation(() => result);
      expect(await controller.create(result)).toBe(result);
    });
  });

  describe('delete', () => {
    it('should delete a customer', async () => {
      const result = null;

      jest.spyOn(service, 'remove').mockImplementation(() => result);
      expect(await controller.remove('3')).toBe(result);
    });
  });
});
