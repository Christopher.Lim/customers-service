/* eslint-disable @typescript-eslint/no-unused-vars */
import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { CustomersService } from './customers.service';

describe('CustomersService', () => {
  let service: CustomersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CustomersService],
    }).compile();

    service = module.get<CustomersService>(CustomersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    describe('when making  /customer request', () => {
      it('should return the customer list', async () => {
        const expectedCustomer = {
          id: 1,
          firstName: 'Chris',
          lastName: 'Lim',
          title: 'Test-Ops',
          location: ['USA', 'Philippines'],
        };

        const customer = await service.findAll();
        expect(customer).toContainEqual(expectedCustomer);
      });
    });
  });

  describe('findOne', () => {
    describe('when customer with ID exists', () => {
      it('should return the customer object', async () => {
        const customerId = '1';
        const expectedCustomer = {
          id: 1,
          firstName: 'Chris',
          lastName: 'Lim',
          title: 'Test-Ops',
          location: ['USA', 'Philippines'],
        };

        const customer = await service.findOne(customerId);
        expect(customer).toEqual(expectedCustomer);
      });
    });
    describe('otherwise', () => {
      it('should throw the "NotFoundException"', async () => {
        const customerId = '-1';

        try {
          await service.findOne(customerId);
        } catch (error) {
          expect(error).toBeInstanceOf(NotFoundException);
          expect(error.message).toEqual(`Customer #${customerId} not found`);
        }
      });
    });
  });

  describe('create', () => {
    describe('when successfully created a customer', () => {
      it('should sucessfully remove the customer', async () => {
        const body = {
          id: 5,
          firstName: 'POST',
          lastName: 'Test',
          title: 'Unit Test',
          location: ['Boston'],
        };

        const customer = await service.create(body);
        expect(customer).toEqual(body);
      });
    });
    describe('otherwise', () => {
      it('should throw the "NotFoundException"', async () => {
        const reqBody = {
          firstName: 'POST',
          lastName: 'Test',
          title: 'Unit Test',
          location: ['Boston'],
        };

        const resBody = {
          statusCode: 400,
          message: [
            'id must be a number conforming to the specified constraints',
          ],
          error: 'Bad Request',
        };

        try {
          await service.create(reqBody);
        } catch (error) {
          expect(error).toEqual(resBody);
        }
      });
    });
  });

  describe('remove', () => {
    describe('when customer with ID exists', () => {
      it('should sucessfully remove the customer', async () => {
        const customerId = '1';

        const customer = await service.remove(customerId);
        expect(customer).toBeUndefined();
      });
    });
    describe('otherwise', () => {
      it('should throw the "NotFoundException"', async () => {
        const customerId = '-1';

        try {
          await service.findOne(customerId);
        } catch (error) {
          expect(error).toBeInstanceOf(NotFoundException);
          expect(error.message).toEqual(`Customer #${customerId} not found`);
        }
      });
    });
  });
});
