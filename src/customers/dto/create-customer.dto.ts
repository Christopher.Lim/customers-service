import { IsNumber, IsString } from 'class-validator';

export class CreateCustomerDto {
  @IsNumber()
  readonly id: number;

  @IsString()
  readonly firstName: string;

  @IsString()
  readonly lastName: string;

  @IsString()
  readonly title: string;

  @IsString({ each: true })
  readonly location: string[];
}
